#!/bin/bash

set -ex

# a quick test
hdsploader -h
if [ $? -ne 0 ]; then
  echo "The return code from hdsploader is invalid ($?)!"
  exit 99
fi
